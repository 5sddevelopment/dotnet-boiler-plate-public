﻿using System;
using System.Collections.Generic;

namespace Boiler_Plate.Data;

public partial class Right
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string Description { get; set; } = null!;

    public bool? IsActive { get; set; }
}
