﻿using System;
using System.Collections.Generic;

namespace Boiler_Plate.Data;

public partial class Otp
{
    public int Id { get; set; }

    public int UserId { get; set; }

    public string Code { get; set; } = null!;

    public DateTime CreatedDate { get; set; }

    public virtual User User { get; set; } = null!;
}
