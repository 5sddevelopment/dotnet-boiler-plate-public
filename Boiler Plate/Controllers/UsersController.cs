﻿using Boiler_Plate.Data;
using Boiler_Plate.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Diagnostics.Metrics;
using System.Text.Json;
using Boiler_Plate.Common;

namespace Boiler_Plate.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private BoilerPlateContext dbContext;
        private readonly ILogger<UsersController> _logger;
        private readonly MailController _mailController;
        public UsersController(BoilerPlateContext dbContext, ILogger<UsersController> logger, MailController mailController)
        {
            this.dbContext = dbContext;
            _logger = logger;   
            _mailController = mailController;
        }

        #region User CRUD Operations

        [HttpPost("Create")]
        public async Task<IActionResult> CreateUser([FromBody] User model)
        {
            _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(CreateUser), JsonSerializer.Serialize(model));

            var user = new User()
            {
                FirstName = model.FirstName,
                MiddleName = model.MiddleName,
                LastName = model.LastName,
                PhoneNumber = model.PhoneNumber,
                City = model.City,
                Country = model.Country,
                Email = model.Email,
                Password = EncryptPassword(model.Password),
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow,
            };
            await dbContext.Users.AddAsync(user);
            await dbContext.SaveChangesAsync();
            return Ok(model);
        }

        [HttpGet("GetUser")]
        public async Task<IActionResult> GetUser([FromQuery] int Id)
        {
            _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(GetUser), JsonSerializer.Serialize(Id));
            var user = dbContext.Users.FirstOrDefault(x => x.Id == Id);
            if (user is not null)
            {
                return Ok(user);
            }
            return NotFound();
        }

        [HttpGet("GetAllUsers")]
        public async Task<IActionResult> GetAllUsers()
        {
            _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(GetAllUsers), "Getting all users");
            return Ok(await dbContext.Users.ToListAsync());
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update([FromBody] User model)
        {
            _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(Update), JsonSerializer.Serialize(model));
            var user = dbContext.Users.FirstOrDefault(x => x.Id == model.Id);

            if (user != null)
            {
                user.FirstName = model.FirstName;
                user.MiddleName = model.MiddleName;
                user.LastName = model.LastName;
                user.PhoneNumber = model.PhoneNumber;
                user.City = model.City;
                user.Country = model.Country;
                user.Email = model.Email;
                user.Password = EncryptPassword(model.Password);
                user.UpdatedDate = DateTime.UtcNow;

                dbContext.SaveChanges();
            }
            return Ok(model);
        }

        [HttpDelete("Delete")]
        public async Task<IActionResult> Delete([FromQuery] int Id)
        {
            _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(Delete), JsonSerializer.Serialize(Id));
            var user = dbContext.Users.FirstOrDefault(x => x.Id == Id);
            if (user is not null) 
            {
                dbContext.Remove(dbContext.Users.Single(d => d.Id == Id));
                dbContext.SaveChanges();
            }
            return Ok("User has been deleted");
        }

        #endregion

        
        [HttpGet("Login")]
        public async Task<IActionResult> Login([FromBody] string email, string password)
        {
            var encpassword = EncryptPassword(password);
            var findUser = await dbContext.Users.Select(x => x.Email == email && x.Password == encpassword).FirstOrDefaultAsync();
            if(findUser == null)
            {
                return NotFound();
            }
            
            return Ok("Login Success!");
        }

        #region Forgot Password
        [HttpGet("ForgotPassword/GetEmail")]
        public async Task<IActionResult> GetEmail([FromQuery] string email)
        {
            var findUser = await dbContext.Users.SingleAsync(x => x.Email == email);
            if(findUser == null)
            {
                return NotFound();
            }

            int Otp = Common.Common.GenerateRandomNo();

            MailRequest mailRequest = new MailRequest()
            {
                ToEmail = email,
                Body = String.Format("Your Verification OTP is: {0}", Otp),
                Subject = "Forget Password"
            };

            _mailController.SendEmailAsync(mailRequest);

            var Userotp = new Otp()
            {
                UserId = findUser.Id,
                Code = Otp.ToString(),
                CreatedDate = DateTime.UtcNow,
            };
            await dbContext.Otps.AddAsync(Userotp);
            await dbContext.SaveChangesAsync();

            return Ok();
        }

        [HttpGet("ForgotPassword/VerifyOtp")]
        public async Task<IActionResult> VerifyOtp([FromQuery] string Otp, int UserId)
        {
            var findUser = await dbContext.Otps.SingleAsync(x => x.UserId == UserId && x.Code == Otp && DateTime.Now.Subtract(x.CreatedDate).TotalMinutes <= 2);
            if (findUser is null)
                return NotFound();

            return Ok("Verified!");
        }

        [HttpGet("ForgotPassword/NewPassword")]
        public async Task<IActionResult> NewPassword([FromQuery] string Password, int UserId)
        {
            _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(NewPassword), JsonSerializer.Serialize(UserId));
            var user = dbContext.Users.FirstOrDefault(x => x.Id == UserId);

            if (user != null)
            {
                user.Password = EncryptPassword(Password);
                user.UpdatedDate = DateTime.UtcNow;
                dbContext.SaveChanges();
            }

            return Ok();
        }

        #endregion

        #region Roles and Rights

        [HttpPost("Role")]
        public async Task<IActionResult> AddRole([FromBody] Role model)
        {
            _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(AddRole), JsonSerializer.Serialize(model));
            var role = new Role()
            {
                Name = model.Name,
                Description = model.Description,
                IsActive = model.IsActive,
            };
            await dbContext.Roles.AddAsync(role);
            await dbContext.SaveChangesAsync();
            return Ok(model);
        }

        [HttpPost("Rights")]
        public async Task<IActionResult> AddRights([FromBody] Right model)
        {
            _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(AddRights), JsonSerializer.Serialize(model));
            var right = new Right()
            {
                Name = model.Name,
                Description = model.Description,
                IsActive = model.IsActive,
            };
            await dbContext.Rights.AddAsync(right);
            await dbContext.SaveChangesAsync();
            return Ok(model);
        }

        [HttpPost("AssignRole")]
        public async Task<IActionResult> AssignRole([FromBody] UserRole model)
        {
            _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(AssignRole), JsonSerializer.Serialize(model));
            var userRole = new UserRole()
            {
                UserId = model.UserId,
                RoleId = model.RoleId,
            };
            await dbContext.UserRoles.AddAsync(userRole);
            await dbContext.SaveChangesAsync();
            return Ok(model);
        }

        [HttpPost("AssignRights")]
        public async Task<IActionResult> AssignRights([FromBody] RoleRight model)
        {
            _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(AssignRights), JsonSerializer.Serialize(model));
            var roleRight = new RoleRight()
            {
                RoleId = model.RoleId,
                RightId = model.RightId,
            };
            await dbContext.RoleRights.AddAsync(roleRight);
            await dbContext.SaveChangesAsync();
            return Ok(model);
        }

        [HttpGet("Privileges")]
        public async Task<IActionResult> GetPrivileges([FromQuery] int UserId)
        {
            _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(GetPrivileges), JsonSerializer.Serialize(UserId));
            var privileges = from users in dbContext.Set<User>()
                        join roles in dbContext.Set<UserRole>()
                            on users.Id equals roles.UserId
                        join rolerights in dbContext.Set<RoleRight>()
                            on roles.RoleId equals rolerights.RoleId
                        where users.Id == UserId
                        select new { users, roles, rolerights };
            if (privileges == null)
            {
                return NotFound("No privilege found against this User.");
            }
            return Ok(privileges);
        }

        #endregion

        #region PrivateFunctions
        private string EncryptPassword(string password)
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes(password);
            data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
            String hash = System.Text.Encoding.ASCII.GetString(data);
            return hash;
        }
        #endregion


    }
}
