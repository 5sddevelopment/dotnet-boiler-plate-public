﻿using System;
using System.Collections.Generic;

namespace Boiler_Plate.Data;

public partial class RoleRight
{
    public int RoleId { get; set; }

    public int RightId { get; set; }

    public virtual Right Right { get; set; } = null!;

    public virtual Role Role { get; set; } = null!;
}
